const initialState = {
    numbers: '',
    stars: '',
    shadows: 'none',
    message: '',
    messageDisplay: 'none',
};

const reducer = (state = initialState, action) => {
    if (action.type === 'ADD') {
        return  {
            ...state,
            numbers: state.numbers + action.value,
            stars: state.stars + "*",
        };
    }
    if (action.type === 'DELETE') {
        return {
            ...state,
            numbers: state.numbers.substring(0, state.numbers.length - 1),
            stars: state.stars.substring(0, state.stars.length - 1),
            messageDisplay: 'none',
            shadows: 'none',
        };
    }
    if (action.type === 'TRUE') {
        return {
            ...state,
            shadows: action.value,
            message: 'Access Granted',
            messageDisplay: 'block',
        }
    }
    if (action.type === 'FALSE') {
        return {
            ...state,
            shadows: action.value,
            message: 'Access Denied',
            messageDisplay: 'block',
        }
    }
    return state;
};

export default reducer;