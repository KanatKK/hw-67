import React from 'react';
import './pin.css';
import {useSelector, useDispatch} from "react-redux";

const Pin = () => {
    const password = '8888';
    const deleteSign = '<';

    const nums = useSelector(state => state.numbers);
    const stars = useSelector(state => state.stars);
    const shadows = useSelector(state => state.shadows);
    const message = useSelector(state => state.message);
    const messageDisplay = useSelector(state => state.messageDisplay);
    const dispatch = useDispatch();

    const add = event => {
        if (nums.length < 4) {
            dispatch({type: 'ADD', value: event.target.innerHTML});
        }
    };

    const deleteNum = () => dispatch({type: 'DELETE'});

    const verification = () => {
        if (nums === password) {
            dispatch({type: 'TRUE', value: 'inset -1px -1px 5px 5px rgba(0, 255, 70, 0.5)'});
        } else {
            dispatch({type: 'FALSE', value: 'inset -1px -1px 5px 5px rgba(250, 0, 0, 0.5)'});
        }
    };

    const disabled = nums.length !== 4;

    return (
        <div className="pin">
            <div className="display" style={{boxShadow: shadows}}>
                <p className="message" style={{display: messageDisplay}}>{message}</p>
                <p className='stars'>{stars}</p>
            </div>
            <div className="buttons">
                <div className="row">
                    <button type="button" onClick={add} className="num">7</button>
                    <button type="button" onClick={add} className="num">8</button>
                    <button type="button" onClick={add} className="num">9</button>
                </div>
                <div className="row">
                    <button type="button" onClick={add} className="num">4</button>
                    <button type="button" onClick={add} className="num">5</button>
                    <button type="button" onClick={add} className="num">6</button>
                </div>
                <div className="row">
                    <button type="button" onClick={add} className="num">1</button>
                    <button type="button" onClick={add} className="num">2</button>
                    <button type="button" onClick={add} className="num">3</button>
                </div>
                <div className="row">
                    <button type="button" onClick={deleteNum} className="num">{deleteSign}</button>
                    <button type="button" onClick={add} className="num">0</button>
                    <button type="button" onClick={verification} disabled={disabled} className="num">E</button>
                </div>
            </div>
        </div>
    );
};

export default Pin;